use std::collections::HashMap;
use std::fs::File;
use serde::{Serialize, Deserialize};
use serde_yaml;
use std::process::Command;

#[derive(Serialize, Deserialize, Debug)]
struct Config {
    runtime: String,
    scripts: HashMap<String, String>,
}


fn main() -> Result<(), std::io::Error> {
    println!("Welcome to runpy.");
    let workingdir = std::env::current_dir()?;

    println!("{}", workingdir.display());


    let args: Vec<String> = std::env::args().collect();
    println!("{:?}", args);

    let config = load_config();

    let script_name: &str = args[1].as_str();

    let path_to_py_module = config.scripts.get(script_name).unwrap();

    println!();
    println!("PATH TO PY MODULE");
    println!("{:?}", path_to_py_module);
    println!();

    let mut py_runtime_command = Command::new(config.runtime);

    //py_runtime_command.arg("script_3/app.py");
    //py_runtime_command.arg("test");

    let args = shell_words::split(path_to_py_module).expect("Error while parsing arguments");

    py_runtime_command.args(args);

    println!("{:?}", py_runtime_command.get_program());
    println!("{:?}", py_runtime_command.get_args());

    let output = py_runtime_command.output().expect("Failed!");

    let printable_output = String::from_utf8(output.stdout).unwrap();

    println!("{}", printable_output);

    Ok(())
}

fn load_config() -> Config {
    let config_file = File::open("runpy.yaml").expect("'runpy.yaml' not found.");
    let config: Config = serde_yaml::from_reader(config_file).expect("Could not read 'runpy.yaml'");

    println!("{:?}", config);

    config
}
